import sys

from PyQt5.QtWidgets import QApplication

from ui.main_window import SimpleWindow

if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = SimpleWindow()
    sys.exit(app.exec_())
