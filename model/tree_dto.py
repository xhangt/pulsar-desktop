class TreeDTO:
    TYPE_ROOT = "root"
    TYPE_CLUSTER = "cluster"
    TYPE_TENANT = "tenant"

    def __init__(self, id, name, type):
        self.id = id
        self.name = name
        self.type = type

    def __init__(self, id, name, type, child=None):
        self.id = id
        self.name = name
        self.type = type
        self.child = child
