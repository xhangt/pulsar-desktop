from dataclasses import dataclass


@dataclass
class PulsarCluster:

    def __init__(self, id=None, name=None, pulsar_addr=None, admin_addr=None):
        self.id = id
        self.name = name
        self.pulsar_addr = pulsar_addr
        self.admin_addr = admin_addr

    def __str__(self):
        return f"PulsarCluster(id={self.id}, name={self.name}, pulsar_addr={self.pulsar_addr}, admin_addr={self.admin_addr})"
