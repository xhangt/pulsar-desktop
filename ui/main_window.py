from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QMainWindow, QVBoxLayout, \
    QWidget, QSplitter

from ui.cluster_tree import SideBar
from ui.main_content_area import MainContentArea
from ui.manu_bar import MenuBar


class SimpleWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        # 初始化菜单栏
        self.menu = MenuBar()
        self.setMenuBar(self.menu.menuBar())

        # 创建侧栏和主内容区域
        self.main_content_area = MainContentArea()
        self.cluster_side_bar = SideBar(self)

        # 使用 QSplitter 将侧栏和主窗口内容分开
        splitter = QSplitter(Qt.Horizontal)
        splitter.addWidget(self.cluster_side_bar)
        splitter.setStretchFactor(1, 1)  # 设置右侧区域扩展优先级

        self.mainSpliter = QSplitter(Qt.Horizontal)
        self.mainSpliter.addWidget(splitter)
        self.mainSpliter.addWidget(self.main_content_area)
        self.mainSpliter.setStretchFactor(1, 1)

        # 创建一个中心窗口小部件
        centralWidget = QWidget(self)
        layout = QVBoxLayout(centralWidget)
        layout.addWidget(self.mainSpliter)
        self.setCentralWidget(centralWidget)
        self.setGeometry(300, 300, 1200, 1000)
        self.setWindowTitle('pulsar-admin-UI')
        self.show()
