from PyQt5.QtWidgets import QMainWindow, QAction, qApp


class MenuBar(QMainWindow):
    def __init__(self):
        super().__init__()
        self.initMenuBar()

    def initMenuBar(self):
        # 创建菜单栏
        menubar = self.menuBar()

        # 创建菜单
        fileMenu = menubar.addMenu('文件')
        editMenu = menubar.addMenu('编辑')
        viewMenu = menubar.addMenu('视图')
        helpMenu = menubar.addMenu('帮助')

        # 创建菜单项
        exitAction = QAction('退出', self)
        exitAction.setShortcut('Ctrl+Q')
        exitAction.setStatusTip('退出应用')
        exitAction.triggered.connect(qApp.quit)

        # 将菜单项添加到菜单
        fileMenu.addAction(exitAction)
