from PyQt5.QtWidgets import QSystemTrayIcon, QStyle


class NotificationManager:
    def __init__(self, parent=None):
        self.tray_icon = QSystemTrayIcon(parent)
        self.tray_icon.setIcon(parent.style().standardIcon(QStyle.SP_MessageBoxCritical))

    def show_error(self, title, message):
        self.tray_icon.showMessage(title, message, QSystemTrayIcon.Critical)
