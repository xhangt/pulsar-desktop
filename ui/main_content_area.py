import json

from PyQt5.QtWidgets import QVBoxLayout, QTextBrowser


class MainContentArea(QTextBrowser):
    def __init__(self):
        super().__init__()
        self.initTable()

    def initTable(self):
        self.setText("")

        # 设置布局
        layout = QVBoxLayout(self)
        layout.addWidget(self)
        self.setLayout(layout)

    def refreshText(self, text):
        try:
            self.setText(json.dumps(json.loads(text), indent=4))
            return
        except Exception as e:
            pass
        self.setText(text)
