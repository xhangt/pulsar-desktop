from PyQt5.QtWidgets import QDialog, QLabel, QLineEdit, QPushButton, QVBoxLayout

class EditDialog(QDialog):
    def __init__(self, parent=None, name="", pulsar_addr="", admin_addr="", cluster_id=None):
        super().__init__(parent)
        self.setWindowTitle("编辑节点")
        self.name_edit = QLineEdit(name)
        self.pulsar_addr_edit = QLineEdit(pulsar_addr)
        self.admin_addr_edit = QLineEdit(admin_addr)
        self.save_button = QPushButton("保存")
        self.cancel_button = QPushButton("取消")
        self.save_button.clicked.connect(self.save)
        self.cancel_button.clicked.connect(self.cancel)
        layout = QVBoxLayout()
        layout.addWidget(QLabel("名称:"))
        layout.addWidget(self.name_edit)
        layout.addWidget(QLabel("Pulsar地址: pulsar://host:port"))
        layout.addWidget(self.pulsar_addr_edit)
        layout.addWidget(QLabel("管理地址: http://host:port"))
        layout.addWidget(self.admin_addr_edit)
        layout.addWidget(self.save_button)
        layout.addWidget(self.cancel_button)
        self.setLayout(layout)
        self.cluster_id = cluster_id  # 保存节点的id属性

    def save(self):
        name = self.name_edit.text()
        pulsar_addr = self.pulsar_addr_edit.text()
        admin_addr = self.admin_addr_edit.text()
        # 在这里调用某个方法，传入name、pulsar_addr、admin_addr和id
        self.accept()

    def cancel(self):
        self.reject()
